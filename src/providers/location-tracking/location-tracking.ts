import { HttpClient } from '@angular/common/http';
import { Injectable, NgZone, EventEmitter } from '@angular/core';
import { BackgroundGeolocation, BackgroundGeolocationConfig, BackgroundGeolocationResponse } from '@ionic-native/background-geolocation';
import { Geolocation, Geoposition, GeolocationOptions } from "@ionic-native/geolocation";
import "rxjs/add/operator/filter";
import { Subscription } from "rxjs";
const REVERSE_GEO_CODE_URI = "https://nominatim.openstreetmap.org/reverse?format=json";
export interface CurrentLoc {
  latitude: number;
  longitude: number;
}


@Injectable()
export class LocationTrackingProvider {
  backgroundConfig: BackgroundGeolocationConfig;
  foregroundConfig: GeolocationOptions;
  watchForegroundPos: Subscription;
  locationUpdated:any = new EventEmitter<any>();
  currentLoc: CurrentLoc = {
    latitude: 0,
    longitude: 0,
  }
  constructor(
    public http: HttpClient,
    private zone: NgZone,
    private bgGeo: BackgroundGeolocation,
    private geo: Geolocation
  ) {
    this.initGeoConfigs();
  }
  initGeoConfigs() {
    this.backgroundConfig = {
      desiredAccuracy: 0,
      stationaryRadius: 20,
      distanceFilter: 10,
      interval: 1000
    }
    this.foregroundConfig = {
      enableHighAccuracy: true
    }
  }
   startTracking() {
    this.backgroundTracking();
    this.foregroundTracking();
  }
  foregroundTracking() {
    console.log("[FOREGROUND TRACKING STARTED]");
    this.watchForegroundPos  = this.geo.watchPosition(this.foregroundConfig)
      .filter((p: any) => 'undefined' === typeof p.code)
      .subscribe((position: Geoposition) => {
        const { latitude, longitude } = position.coords;
        console.log("[FOREGROUND TRACKING LOCATION]", { latitude, longitude});

        this.handleGeoPositioning({latitude, longitude});
      })
  }
  backgroundTracking() {
    console.log("[BACKGROUND TRACKING STARTED]");
    this.bgGeo.configure(this.backgroundConfig)
      .subscribe(
      (loc: BackgroundGeolocationResponse) => {
        const { latitude, longitude} = loc;
        console.log("[BACKGROUND TRACKING LOCATION]", { latitude, longitude});

        this.handleGeoPositioning({latitude, longitude});
      },
      (err) => this.handleTrackingError(err)
    );
    this.bgGeo.start();
  }
  handleGeoPositioning(loc: CurrentLoc) {
    this.zone.run(() => {
      this.currentLoc = loc;
      this.locationUpdated.emit(this.currentLoc);
    })
  }
  stopTracking() {
    this.bgGeo.stop();
    if (this.watchForegroundPos) {
      this.watchForegroundPos.unsubscribe();
    }
  }
  handleTrackingError(error) {
    console.log("[TRACKING ERROR]", error);
  }

  reverseGeocoding(loc: CurrentLoc): Promise<any> {
    return new Promise((resolve, reject) => {
      const { latitude, longitude } = loc;
      this.http.get(`${REVERSE_GEO_CODE_URI}&lat=${latitude}&lon=${longitude}`)
      .subscribe((response) => {
        resolve(response);
      }, (err) => reject(err))
    })
  }
}

import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Contacts, ContactFieldType } from "@ionic-native/contacts";
export interface IProfile {
  firstName?: string;
  lastName?: string;
  udid?: string;
}
@Injectable()
export class ProfileProvider {
  private userProfile: IProfile = {
    firstName: null,
    lastName: null,
    udid: null
  };
  constructor(private contactsSrv: Contacts) {}

  async setProfile(profile: IProfile): Promise<any> {
    return new Promise((resolve, reject) => {
      try {
        const profileObj = Object.assign(this.userProfile, profile);
        localStorage.setItem("profile", JSON.stringify(profileObj));
        resolve("OK");
      } catch (error) {
        console.log("[SET PROFILE ERROR]:", error);
        reject(error);
      }
    });
  }

  async getProfile(): Promise<IProfile> {
    return new Promise((resolve, reject) => {
      try {
        const profile = <IProfile>JSON.parse(localStorage.getItem("profile"));
        resolve(profile);
      } catch (error) {
        reject(error);
      }
    });
  }

  getContacts() {
    const contacts = localStorage.getItem("contacts");
    if (contacts) {
      return JSON.parse(contacts);
    }
    return [];
  }

  addContact(contactObjs: any[]) {
    localStorage.setItem("contacts", JSON.stringify(contactObjs));
  }
}

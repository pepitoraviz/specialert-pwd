import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { Contacts } from '../contact/contact';
import { LocationTrackingProvider, CurrentLoc, ProfileProvider, IProfile } from '../../providers';
import { Socket } from 'ng-socket-io';
import { SMS } from '@ionic-native/sms';

const ALERT = 'alert';
const REGISTER = 'register';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  isDanger: boolean = false;
  alertMessage: string;
  interval: any;
  currentAddress: string;
  profile: IProfile;
  broadcastInfo: any = {};
  defaultContact: Contacts;

  constructor(
    private ltServ: LocationTrackingProvider,
    private platform: Platform,
    private socket: Socket,
    private profileSrv: ProfileProvider,
    private sms: SMS
  ) {
    this.alertMessage = 'Broadcast alert'; //default message
    this.initProfile();
  }
  async initProfile() {
    this.profile = await this.profileSrv.getProfile();
    const contacts = await this.profileSrv.getContacts();
    const contactIndexes = contacts.map(contact => contact.default).indexOf(true);
    if (-1 !== contactIndexes) {
      this.defaultContact = contacts[contactIndexes];
    }
  }
  ionViewDidEnter() {
    this.ltServ.locationUpdated.subscribe(async (loc: CurrentLoc) => {
      console.log('loc: ', loc);
      try {
        const pos = { lat: loc.latitude, lng: loc.longitude };
        this.broadcastInfo = { profile: this.profile, pos };
      } catch (error) {
        console.log('[ERROR GETTING CURRENT ADDRESS]: ', error);
      }
    });
  }
  ionViewDidLoad() {
    this.socket.emit(REGISTER, this.profile);
  }
  toggleDanger() {
    this.isDanger = !this.isDanger;
    this.toggleAlertMessage();
    this.setBroadCastInterval();
    this.platform.ready().then(() => {
      if (this.isDanger) {
        this.sms.send(
          this.defaultContact.contactNumber,
          'Im in danger. Please call some help ASAP'
        );
        this.ltServ.startTracking();
      } else {
        this.ltServ.stopTracking();
      }
    });
  }

  toggleAlertMessage(): void {
    this.alertMessage = this.isDanger ? 'Stop alert ' : 'Broadcast alert';
  }
  setBroadCastInterval(): void {
    if (this.isDanger) {
      this.interval = setInterval(() => this.broadcastLatLng(), 5000);
    } else {
      clearInterval(this.interval);
    }
  }

  broadcastLatLng(): void {
    console.log('[START EMIT]');
    if (Object.keys(this.broadcastInfo).length > 0) {
      this.socket.emit(ALERT, this.broadcastInfo);
    }
  }

  startTracking() {
    this.ltServ.startTracking();
  }
}

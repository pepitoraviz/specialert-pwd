import { Component, ViewChild } from '@angular/core';
import { ProfileProvider } from '../../providers';
import { Slides, NavController } from 'ionic-angular';
import { UniqueDeviceID } from '@ionic-native/unique-device-id';
import { TabsPage } from '../';
import { Contacts } from '../contact/contact';

@Component({
  selector: 'page-onboard',
  templateUrl: 'on-board.html'
})
export class OnBoardPage {
  @ViewChild(Slides)
  slides: Slides;
  firstName: string;
  lastName: string;
  contactName: string;
  contactNumber: string;
  contacts: Contacts[] = [];
  constructor(
    private profileSrv: ProfileProvider,
    private udidSrv: UniqueDeviceID,
    private navCtrl: NavController
  ) {}

  ionViewDidLoad() {
    this.slides.lockSwipes(true);
  }
  async registerDeviceId() {
    const udid = await this.udidSrv.get();
    await this.profileSrv.setProfile({ udid });
    this.slideNext();
  }

  slideChanged() {
    const slideInd = this.slides.getActiveIndex();

    if (2 === slideInd) {
      this.profileSrv.getContacts();
    }
  }
  slideNext() {
    this.slides.lockSwipes(false);

    this.slides.slideNext(500);
    this.slides.lockSwipes(true);
  }
  async storeUserFullName() {
    try {
      await this.profileSrv.setProfile({ firstName: this.firstName, lastName: this.lastName });
      this.navCtrl.setRoot(TabsPage);
    } catch (error) {
      console.log(error);
    }
  }
  async saveContact() {
    try {
      const contact: Contacts = {
        contactName: this.contactName,
        contactNumber: this.contactNumber,
        default: true
      };
      this.contacts.push(contact);
      await this.profileSrv.addContact(this.contacts);
      this.slideNext();
    } catch (error) {
      console.log(error);
    }
  }
}

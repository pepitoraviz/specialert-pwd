import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { Flashlight } from "@ionic-native/flashlight";
import { NativeAudio } from '@ionic-native/native-audio';
interface IAudioObj {
    displayText?: string;
    alias: string;
    path: string;
}

@Component({
  selector: 'page-tools',
  templateUrl: 'tools.html'
})
export class ToolsPage {
    playingAudio:boolean = false;
    flashlightAvailable: boolean;
    strobleInterval: number;
    flashLightOn: boolean;
    preLoadedAudio: string;
    audioObjs: IAudioObj[];
     constructor(
        private platform: Platform,
        private flashLight: Flashlight,
        private nativeAudio: NativeAudio
    ){
         this.isFlashlightAvailable();
         this.audioObjs = [
             { alias:'help', path: 'assets/media/help.mp3', displayText: "Help"},
             { alias:'danger', path: 'assets/media/danger.mp3', displayText: "Danger"},
             { alias:'emergency', path: 'assets/media/emergency.mp3', displayText: "Emergency"},
             { alias:'assistance', path: 'assets/media/assistance.mp3', displayText: "Assistance"},
         ];
    }
    async isFlashlightAvailable(): Promise<any> {
         await this.platform.ready()
            .then(async () => {
                this.flashlightAvailable = await this.flashLight.available();
         });
    }

    async toggleFlash(){
        if (this.strobleInterval) {
            clearInterval(this.strobleInterval);
            this.strobleInterval = 0;
            this.flashLight.switchOff();
        } else {
            this.strobeFlashLight();
        }
    }
    strobeFlashLight() {
        this.strobleInterval = setInterval(() => this.flashLight.toggle(), 50);
    }


    async playAudio(alias: string, path:string) {
        const audioObj: IAudioObj = { alias, path };
        try {
            await Promise.all([this.stopAndUnload(), this.preLoadAudio(audioObj)]);
            this.nativeAudio.loop(alias);
            this.playingAudio = true;
        } catch (error) {
            console.log("[PLAYING AUDIO FAILED]", error);
        }
    }

    preLoadAudio(audioObj: IAudioObj ): Promise<any> {
        console.log('audioObj: ', audioObj);
        const { alias, path } = audioObj;
        return new Promise(async (resolve, reject) => {
            try {
                await this.nativeAudio.preloadSimple(alias, path) ;
                this.preLoadedAudio = alias;
                resolve();
            } catch (error) {
                console.log("[PRE LOAD AUDIO ERROR]:", error);
                reject(error);
            }
        })
    }
     stopAndUnload(): Promise<any>{
        return new Promise(async (resolve, reject) => {
            try {
                if (this.preLoadedAudio) {
                    await this.nativeAudio.stop(this.preLoadedAudio);
                    await this.nativeAudio.unload(this.preLoadedAudio);
                    this.preLoadedAudio = null;
                    this.playingAudio = false;
                    return resolve();
                }
                return resolve();
            } catch (error) {
                console.log("[STOP AND UNLOAD ERR]:", error);
                reject(error);
            }
        })
    }
}

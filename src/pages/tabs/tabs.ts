import { Component } from "@angular/core";
import { ContactPage, HomePage, MePage, SpeechPage, ToolsPage, } from "../";
import { MessagePage } from '../message/message';

@Component({
	templateUrl: "tabs.html"
})
export class TabsPage {
	homePageRoot = HomePage;
	contactPageRoot = ContactPage;
	mePageRoot = MePage;
	speechPageRoot = SpeechPage;
	toolsPageRoot = ToolsPage;
	messagePageRoot = MessagePage;
	constructor() {
	}
}

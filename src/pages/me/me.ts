import { Component } from "@angular/core";
import { ProfileProvider, IProfile } from "../../providers";
@Component({
  selector: "page-me",
  templateUrl: "me.html"
})
export class MePage {
  encryptMe: string;
  _profile: IProfile;
  constructor(private profileSrv: ProfileProvider) {
    this.initUdId();
  }

  async initUdId() {
    try {
      this._profile = await this.profileSrv.getProfile();
      this.encryptMe = JSON.stringify(this._profile);
    } catch (error) {
      console.log("[ERROR GETTING DEVICE ID]", error);
    }
  }
}

import { Component } from '@angular/core';
import { IonicPage, AlertController } from 'ionic-angular';
import { SMS } from '@ionic-native/sms';
import { ProfileProvider } from '../../providers';
import { Contacts } from '../contact/contact';

@IonicPage()
@Component({
  selector: 'page-message',
  templateUrl: 'message.html',
})
export class MessagePage {
  constructor(
    private sms: SMS,
    private alertCtrl: AlertController,
    private profileServ: ProfileProvider
  ) {

  }

  toggleFlash() {

  }

  showMessageSent() {
    const alrt = this.alertCtrl.create({
      title: 'Success',
      message: 'Alert successfully sent'
    });

    alrt.present();
  }


  sendSms(opts) {
    const messages = [
      'Im on my way home.',
      'Please help me. I need some assistance',
      'Im in danger. Please check my location and call some help ASAP.'
    ];
    const emergencyMessage = messages[opts];
    const contacts: Contacts[] = this.profileServ.getContacts();

    if (Array.isArray(contacts) && contacts.length) {
      contacts.forEach(async (contact) => {
        await this.deliverSMS(contact.contactNumber, emergencyMessage);
      })
      this.showMessageSent();
    }
  }


  deliverSMS(receipientNum: any, message) {
    var options: {
      replaceLineBreaks: true,
      android: {
        intent: 'INTENT'
      }
    }
    if ('undefined' !== receipientNum) {
      return new Promise((resolve, reject) => {
        this.sms.send(receipientNum, message, options)
          .then(() => resolve())
          .catch(() => reject())
      })
    }
    return null;
  }


}

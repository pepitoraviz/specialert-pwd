import { Component } from '@angular/core';
import { TextToSpeech } from '@ionic-native/text-to-speech';

interface ITTS {
  text: string;
  speech: string;
}

@Component({
  selector: 'page-speech',
  templateUrl: 'speech.html'
})

export class SpeechPage {
  preDefinedSpeech: ITTS[];
  customSpeech: string;
    constructor(
      private tts: TextToSpeech
    ){
      this.preDefinedSpeech = [
        { text: "Ask question", speech:"Excuse me , can I ask you a question"},
        { text: "Ask for help", speech:"Excuse me, can you lend me a help"},
        { text: "Ask for street crossing assistance", speech:"Excuse me , can you please help me to cross the street"},
        { text: "Ask for comfort room", speech:"Excuse me , which way is the comfort room"},
      ]
    }

    async triggerTTS(text: any) {
      this.tts.speak(text);
    }
    speakCustomMessage() {
      if (this.customSpeech.length > 0 ) {
        this.triggerTTS(this.customSpeech);

        this.customSpeech = "";
      }
    }

}
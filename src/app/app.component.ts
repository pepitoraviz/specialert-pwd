import { Component } from "@angular/core";
import { Platform } from "ionic-angular";
import { StatusBar } from "@ionic-native/status-bar";
import { SplashScreen } from "@ionic-native/splash-screen";

import { TabsPage, OnBoardPage } from "../pages/";
import { ProfileProvider, IProfile } from "../providers/";

@Component({
  templateUrl: "app.html"
})
export class MyApp {
  rootPage: any;

  constructor(
    platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    private profileServ: ProfileProvider
  ) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
      this.initRootPage();
    });
  }
  async initRootPage() {
    this.profileServ.getProfile().then((profile: IProfile) => {
      if (profile) {
        const { firstName, lastName, udid } = profile;
        if (!firstName || !lastName || !udid) {
          this.rootPage = OnBoardPage;
        } else {
          this.rootPage = TabsPage;
        }
      } else {
        this.rootPage = OnBoardPage;
      }
    });
  }
}

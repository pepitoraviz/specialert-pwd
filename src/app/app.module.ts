import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { HttpClientModule } from '@angular/common/http';
//thirdparty libs
import { QRCodeModule } from 'angularx-qrcode';
import { SocketIoModule, SocketIoConfig } from 'ng-socket-io';

const config: SocketIoConfig = { url: 'https://specialert.herokuapp.com' };
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {
  ContactPage,
  HomePage,
  MePage,
  OnBoardPage,
  SpeechPage,
  TabsPage,
  ToolsPage,
} from '../pages/';
import { UniqueDeviceID } from '@ionic-native/unique-device-id';
//native modules
import { Flashlight } from '@ionic-native/flashlight';
import { NativeAudio } from '@ionic-native/native-audio';
import { TextToSpeech } from '@ionic-native/text-to-speech';
import { BackgroundGeolocation } from '@ionic-native/background-geolocation';
import { Geolocation } from '@ionic-native/geolocation';
import { Contacts } from '@ionic-native/contacts';
import { SMS } from '@ionic-native/sms';
//providers
import { ProfileProvider, LocationTrackingProvider } from '../providers/';
import { ContactProvider } from '../providers/contact/contact';
import { MessagePage } from '../pages/message/message';
@NgModule({
  declarations: [
    MyApp,
    ContactPage,
    HomePage,
    MePage,
    OnBoardPage,
    SpeechPage,
    ToolsPage,
    MessagePage,
    TabsPage
  ],
  imports: [
    BrowserModule,
    SocketIoModule.forRoot(config),
    HttpClientModule,
    QRCodeModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],

  entryComponents: [
    MyApp,
    ContactPage,
    HomePage,
    MePage,
    OnBoardPage,
    SpeechPage,
    MessagePage,
    ToolsPage,
    TabsPage
  ],
  providers: [
    Contacts,
    BackgroundGeolocation,
    Flashlight,
    Geolocation,
    NativeAudio,
    SplashScreen,
    StatusBar,
    TextToSpeech,
    SMS,
    UniqueDeviceID,
    LocationTrackingProvider,
    ProfileProvider,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    ContactProvider
  ]
})
export class AppModule { }
